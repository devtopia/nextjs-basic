/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  /** jestをtypescriptで使うため */
  preset: "ts-jest",
  /** jsdom対応 */
  testEnvironment: "jest-environment-jsdom",
  /** package.jsonから移動 */
  testPathIgnorePatterns: ["<rootDir>/.next", "<rootDir>/node_modules"],
  moduleNameMapper: {
    "\\.(css)$": "<rootDir>/node_modules/jest-css-modules",
    "^axios$": require.resolve("axios")
  },
  /** ts-jestを使った時のエラー対応 */
  transform: {
    "^.+\\.tsx?$": "babel-jest"
  }
};
