import { useRouter } from "next/router";
import React, { ReactElement } from "react";

export default function MultiPage(): ReactElement {
  const router = useRouter();
  const step = router.query.step ?? 0;

  const gotoStep = (_step: number, asPath: string): void => {
    const url: string = `/08_multipage?step=${_step}`;
    const res = router.push(url, asPath);
    console.log(res);
  };

  return (
    <div>
      {Number(step) === 0 && (
        <>
          <h3>Step {step}</h3>
          <button onClick={() => gotoStep(1, "/personal")}>Next Step</button>
        </>
      )}
      {Number(step) === 1 && (
        <>
          <h3>Step {step}</h3>
          <button onClick={() => gotoStep(2, "/confirm")}>Next Step</button>
        </>
      )}
      {Number(step) === 2 && (
        <>
          <h3>Step {step}</h3>
          <button onClick={() => gotoStep(0, "/08_multipage")}>Next Step</button>
        </>
      )}
    </div>
  );
}
