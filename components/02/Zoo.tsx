import React, { ReactElement, useState } from "react";

const animals = ["Lion", "Tiger", "Zebra"];
const Zoo = (): ReactElement => {
  console.log("Rerendering!!!");
  const [keyword, setKeyword] = useState("");
  const search = (e: React.ChangeEvent<HTMLInputElement>): void => {
    console.log(e.target.value);
    setKeyword(e.target.value);
  };
  const animalList = animals
    .filter((animal) => animal.includes(keyword))
    .map((animal) => <li key={animal}>{animal}</li>);
  return (
    <>
      <h3>Zoo:</h3>
      <input type="text" onChange={search} value={keyword} />
      <ul>{animalList}</ul>
    </>
  );
};
export default Zoo;
