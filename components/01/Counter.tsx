import React, { ReactElement, useState } from "react";

const Counter = (): ReactElement => {
  const [counter, setCounter] = useState(0);
  const countUp = (): void => {
    setCounter(counter + 1);
  };
  const countDown = (): void => {
    setCounter(counter - 1);
  };
  return (
    <>
      <h3>Counter: {counter}</h3>
      <p>
        Up: <button onClick={countUp}>+</button>
        &nbsp; Down: <button onClick={countDown}>-</button>
        &nbsp; Reset: <button onClick={() => setCounter(0)}>Reset</button>
      </p>
    </>
  );
};
export default Counter;
